import os
import logging
import pandas as pd
from model.hyper_parameter_tuning_class import HyperParameterTuner
logger = logging.basicConfig(level=logging.INFO)

os.environ["NUMEXPR_MAX_THREADS"] = "400"


data_path="./data"

deployment_list=os.listdir(data_path+'/metrics')

if not (os.path.exists(data_path+'/results/')):
    os.mkdir(data_path+'/results/')



#hiperparameter search
for file in deployment_list:
    logging.info(file)
    
    if set(pd.read_csv(data_path+'/metrics/'+file).columns) >= set(['timestamp', 'request_rate', 'timestamp.1', 'success_rate',
       'latency_mean', 'tcp_connections_open', 'tcp_connection_duration_mean',
       'cpu_usage', 'memory_usage']):
        logging.info('Correct set of columns')
        cov=HyperParameterTuner(n_jobs=int(os.environ["NUMEXPR_MAX_THREADS"]),splits=10,deployment=file[:-4]).grid_search(10,12)
        cov.to_csv(data_path+'/results/'+file)


