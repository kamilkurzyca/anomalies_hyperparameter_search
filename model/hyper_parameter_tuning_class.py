from sklearn.preprocessing import MinMaxScaler
from model.anomaly_detection import AnomalyDetector
from model.anomaly_queue import FindAnomalies
from pyod.models.copod import COPOD
import pandas as pd
import numpy as np

from sklearn.model_selection import KFold
from joblib import Parallel, delayed

class HyperParameterTuner:
    
    
   def __init__(self,n_jobs=10,splits=10,path='./data/',deployment='product'):
        self.path=path
        self.n_jobs=n_jobs
        self.deployment=deployment
        self.validation_data=self.prepare_validation()
        self.data=self.prepare_data()
        self.cv_generator=KFold(n_splits=splits)


   def prepare_validation(self):
       scaler = MinMaxScaler()
       path_to_validation=self.path+'validation/golden.csv'
       validation_data=pd.read_csv(path_to_validation)
       validation_data.dropna(thresh=4,inplace=True)
       validation_data.drop('Unnamed: 0',axis=1,inplace=True)
       
       validation_data.replace([np.inf, -np.inf], np.nan, inplace=True)
       validation_data[['errors','latency','saturation_cpu','saturation_memory','traffic']]=scaler.fit_transform(validation_data[['errors','latency','saturation_cpu','saturation_memory','traffic']])

       validation_data.fillna(value=-1,inplace=True)
       return validation_data
   
   def prepare_data(self):
        scaler = MinMaxScaler()
    
        path_to_metric=self.path+'metrics/'+self.deployment+'.csv'
        
        metrics_data=pd.read_csv(path_to_metric)
        metrics_data.replace([np.inf, -np.inf], np.nan, inplace=True)
        metrics_data.dropna(thresh=5,inplace=True)
        metrics_data.drop('timestamp',axis=1,inplace=True)

        metrics_data.rename(columns={'timestamp.1':'timestamp'},inplace=True)
        metrics_data[['request_rate', 'success_rate', 'memory_usage', 'tcp_connections_open',
           'latency_mean', 'cpu_usage', 'tcp_connection_duration_mean']]=scaler.fit_transform(metrics_data[['request_rate', 'success_rate', 'memory_usage', 'tcp_connections_open',
           'latency_mean', 'cpu_usage', 'tcp_connection_duration_mean']])

        metrics_data.fillna(value=-1,inplace=True)

        data=pd.merge(metrics_data,self.validation_data,on=['timestamp'],how='inner')
        data=data.sort_values('timestamp')
        return data




   def create_data_split_and_scale(self,train,test):

        train_data=self.data.iloc[train][['request_rate', 'success_rate', 'memory_usage', 'tcp_connections_open',
           'latency_mean', 'cpu_usage', 'tcp_connection_duration_mean']] 
   
        
        test_data=self.data.iloc[test][['request_rate', 'success_rate', 'memory_usage', 'tcp_connections_open',
           'latency_mean', 'cpu_usage', 'tcp_connection_duration_mean']] 

        validation_data_train=self.data.iloc[train][['traffic', 'latency', 'saturation_cpu',
            'saturation_memory', 'errors']] 
        validation_data_test=self.data.iloc[test][['traffic', 'latency', 'saturation_cpu',
            'saturation_memory', 'errors']]     
        return train_data,test_data,validation_data_train,validation_data_test


   @staticmethod
   def validation_metric(validation_data, anomalies_data):
    
        validation = validation_data.diff()
        validation['anomalies'] = anomalies_data
        validation = validation.dropna()
        cov=validation.cov()
        return cov['anomalies'].drop('anomalies').abs().sum()
    
    
   @staticmethod
   def params_generator(std_iterations,per_iterations):
       std=0
       for j in range(std_iterations):
            std+=0.2
            per=0
            for i in range(per_iterations):
                
                per+=0.1
                yield std,per
    
    
    
   def grid_search(self,std_iterations,per_iterations):   
        covariance=[]
        stdev=[]
        percent=[]
        for std,per in self.params_generator(std_iterations,per_iterations):
 
            cov=Parallel(n_jobs=self.n_jobs)(delayed(self.parallel_grid_search)(train,test,std,per) for train, test in self.cv_generator.split(self.data))

            covariance.append(np.mean(cov))
            percent.append(per)
            stdev.append(std)
        return pd.DataFrame({'covariance':covariance,'percent':percent,'stdev':stdev})



   def parallel_grid_search(self,train,test,std,per):
        train_data,test_data,validation_data_train,validation_data_test=self.create_data_split_and_scale(train,test)
        detector=AnomalyDetector(COPOD(),std,per)
        detector.fit(train_data)
    
        anomalies=FindAnomalies(test_data).find_anomalies_on_data_set(detector)
        return self.validation_metric(validation_data_test, anomalies)

