import tsfel
import multiprocessing
import numpy as np
from joblib import Parallel, delayed


class FindAnomalies:
    '''
    In this class we are making anomaly detection. 
    Input:
        df-dataframe for wich we want to make anomaly detection 
        shift-lenght of signle input of algoritm (time window for single input)
        processes-number of processes which will we running during anomaly detection
    '''
    
    def __init__(self, df, shift=12, processes=10):
        self.processes = processes
        self.df = df
        self.shift = shift
        self.lista = self.prepare_list()
        #self.lista_spectrum = self.prepare_spectrum_list()

    def prepare_list(self):
        return [
            self.df[self.shift + s : 2 * self.shift + s]
            for s in range(self.df.shape[0])
            if 2 * self.shift + s <= self.df.shape[0]
        ]

    def prepare_spectrum_list(self):
        return Parallel(n_jobs=self.processes)(
            delayed(self.feature_extraction)(
                self.df[self.shift + s : 2 * self.shift + s]
            )
            for s in range(self.df.shape[0])
            if 2 * self.shift + s <= self.df.shape[0]
        )

    def feature_extraction(self, time_series):
        '''
        

        Parameters
        ----------
        time_series : DataFrame
            Signal for wich we will process feature decomposition

        Returns
        -------
        TYPE numpy array
            DESCRIPTION: Time series split into features 

        '''
        es = tsfel.get_features_by_domain()
        return tsfel.time_series_features_extractor(
            es, time_series, fs=1, window_size=self.shift, verbose=1, **{"njobs": -1}
        )

    def find_anomalies_on_data_set(self, anomaly_detector):
        '''
        

        Parameters
        ----------
        anomaly_detector : class
            DESCRIPTION: instance of class for anomaly detection

        Returns
        -------
        anomaly_signal : numpy array
            array with number of detected anomalies for each data point. 

        '''
        anomaly_signal = np.zeros(self.df.shape[0])

        results = Parallel(n_jobs=self.processes)(
            delayed(anomaly_detector.find_anomaly)(s) for s in self.lista
        )

        # with multiprocessing.Pool(processes=self.processes) as pool:
        #   results = pool.map(anomaly_detector.find_anomaly, self.lista)

        for index, result in enumerate(results):
            if 2 * self.shift + index <= self.df.shape[0]:
                anomaly_signal[self.shift + index : 2 * self.shift + index] += result
            else:
                break

        return anomaly_signal

    def find_anomalies_on_data_spectrum(self, anomaly_detector, features=None):
        '''
        

        Parameters
        ----------
        anomaly_detector : class
            DESCRIPTION: instance of class for anomaly detection

        Returns
        -------
        anomaly_signal : numpy array
            array with number of detected anomalies for each data point. 

        '''
        anomaly_signal = np.zeros(self.df.shape[0])
        if features != None:
            cut_spectrum_list = [s[features] for s in self.lista_spectrum]

        else:
            cut_spectrum_list = self.lista_spectrum

        results = Parallel(n_jobs=self.processes)(
            delayed(anomaly_detector.find_anomaly_in_spectrum)(s)
            for s in cut_spectrum_list
        )

        # with multiprocessing.Pool(processes=self.processes) as pool:
        #   results = pool.map(anomaly_detector.find_anomaly_in_spectrum, cut_spectrum_list)

        for index, result in enumerate(results):
            if 2 * self.shift + index <= self.df.shape[0]:
                anomaly_signal[self.shift + index : 2 * self.shift + index] += result
            else:
                break

        return anomaly_signal
