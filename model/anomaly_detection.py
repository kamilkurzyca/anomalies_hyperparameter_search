import numpy as np


class AnomalyDetector:
    
     

    '''
    In this class we can detect anomalies in feature selected framework and in simple input framework.
    
    Input:
        clf-instance of anomaly detection algorithm 
        std_treshold- value of standard deviation treshold after witch signal will be claim anomaly
        error_treshold- value of percent of outliers in signal after witch signal will be claim anonaly
    
    '''
    
    def __init__(self, clf, std_treshold, error_treshold):
        self.clf = clf
        self.error_treshold = error_treshold
        self.std_treshold = std_treshold

    def fit(self, X):
        '''
        X-array like signal in wich we want to detect anomalies
        '''
        self.mean_std = X.std().values
        self.clf = self.clf.fit(X)

    def predict(self, X):
        '''
        X-array like signal in wich we want to detect anomalies
        '''
        return self.clf.predict(X)

    def find_anomaly_in_spectrum(self, X, inliner=0, window_size=12):
        '''
        X-array like signal in wich we want to detect anomalies
        
        inliner-format of marking inliners in anomaly detection algorithm
        
        window_size-size of input
        '''
        y_pred = self.predict(X)

        return np.full((window_size,), y_pred[0])

    def find_anomaly(self, X, inliner=0):
        '''
        X-array like signal in wich we want to detect anomalies
        inliner-format of marking inliners in anomaly detection algorithm
        '''
        y_pred = self.predict(X)
        std = X.std().values

        size = X.shape[0]
        if (
            any(std > self.mean_std * self.std_treshold)
            or (y_pred != inliner).sum() / X.shape[0] > self.error_treshold
        ):
            return np.ones(size)

        else:
            return np.zeros(size)
