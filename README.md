# README #

This application is prepared to make a comparison between anomaly detection models with diffrent hyperparameters
This script will iterate true all hyperparameter sets from the config file
Before you run you need to test, train, and validate data in directory ./data
You can find all of them here:
https://console.cloud.google.com/storage/browser/data_from_emporix/model_preparation?project=aiops-269215&pageState=(%22StorageObjectListTable%22:(%22f%22:%22%255B%255D%22))&prefix=&forceOnObjectsSortingFiltering=false


run:
```EXPORT PROCESSES=number_of_processes```
```pip install -r requirements.txt```
```python hyperparameter_tuning.py```


In data, you have data for training and testing. Also, validation data is present. Validation data are golden signals which we are using for model evaluation.
Our evaluation metric is the sum of modules of covariances between the presents of anomaly and golden metrics.
For each deployment, each set of hyperparameters (from config) we are storing: covariance, values of hyperparameters

In this run of experiments we will check 46 deployments from emporix system. We will iterate over standard deviation and percentege of outliers with copod anomaly detection algorithm. We will iterate 11 time percentege with step 0.1 starting from 0 and we will iterate 15 over standard deviation with step 0.2 starting from 0. 

